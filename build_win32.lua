local SDK_DIR = "../source-sdk-2013/"
solution "gmcl_fgt_xyxenthing"

	language "C++"
	platforms { "x32" }
	location ( os.get() .."-".. _ACTION )
	flags { "Symbols", "NoEditAndContinue", "NoPCH", "StaticRuntime", "EnableSSE" }
	targetdir ( "lib/" .. os.get() .. "/" )
	includedirs {	"../gmod-module-base/include",
					SDK_DIR .. "mp/src/public",
					SDK_DIR .. "mp/src/public/tier0",
					SDK_DIR .. "mp/src/public/tier1",
					SDK_DIR .. "mp/src/common",
					SDK_DIR .. "mp/src/game/server",
					SDK_DIR .. "mp/src/game/shared",
				}
	libdirs { SDK_DIR .. "mp/src/lib/public" }

	configurations { "Release" }

	configuration "Release"
		defines { "NDEBUG" }
		flags{ "Optimize", "FloatFast" }

	project "gmcl_fgt_xyxenthing_win32"
		defines { "WIN32", "GMMODULE", "GAME_DLL" }
		links { "vstdlib", "tier2", "tier1", "tier0", "mathlib" }
		files { "src/**.*", "../gmod-module-base/include/**.*" }
		kind "SharedLib"
