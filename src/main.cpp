#if defined( WIN32 )
	#undef _UNICODE
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#elif defined( LINUX )
#endif

#define _ALLOW_KEYWORD_MACROS
#define private public // Call the police, I don't give a fuck.

#include "GarrysMod/Lua/Interface.h"

#include <stdio.h>
#include <cstdlib>
#include <string>
#include <vector>

#include <cbase.h>
#include <interface.h>
#include "eiface.h"
#include <convar.h>

#include "game/server/ientityinfo.h"
#include "props.h"

using namespace GarrysMod::Lua;

IVEngineServer *engine = NULL;

int checkent( lua_State *state ){
	Msg( "Lua Type: %s\n", LUA->GetTypeName(LUA->GetType(1)) );

	if (!engine) {
		Msg( "engine is null!\n" );
		return 0;
	}

	if ( LUA->IsType(1, Type::ENTITY) ){
		UserData *ud = (UserData*)LUA->GetUserdata( 1 );
		Msg( "UserData:\n" );
		Msg( "  Type: [%i] %s\n", ud->type, LUA->GetTypeName(ud->type) );
		Msg( "  Data: 0x%x\n\n", ud->data );

		CBaseHandle *hent = (CBaseHandle*)ud->data;

		if ((hent != (void*)NULL) && hent->IsValid()) {
			int entindex = hent->GetEntryIndex();
			Msg( "Ent Index: %i\n", entindex );

			edict_t *ent = engine->PEntityOfEntIndex( entindex );
			Msg( "edict_t ent: 0x%x\n", ent );

			IServerUnknown *unk = ent->GetUnknown();
			Msg( "IServerUnknown *unk: 0x%x\n", unk );

			if (unk != (void*)NULL) {
				CBaseEntity *uent = unk->GetBaseEntity();
				Msg( "CBaseEntity uent: 0x%x\n", uent );

				Msg( "Casting to CBaseAnimating: " );
				CBaseAnimating *cbase_ent = dynamic_cast<CBaseAnimating*>(uent);
				Msg( "    0x%x\n", cbase_ent );

				Msg( "Casting to CBaseProp: " );
				CBaseProp *cbasep_ent = dynamic_cast<CBaseProp*>(uent);
				Msg( "    0x%x\n", cbasep_ent );
			}
		}

	} else {
		Msg( "Passed argument isn't an entity.\n" );
	}

	return 0;
}

GMOD_MODULE_OPEN() {
	Msg( "fgt_xyxenthing gmod_module_open\n" );

	CreateInterfaceFn ServerInterface = (CreateInterfaceFn)GetProcAddress( GetModuleHandle("engine.dll"), "CreateInterface" );
	Msg( "CreateInterfaceFn ServerInterface: 0x%x\n", ServerInterface );

	engine = (IVEngineServer*)ServerInterface(INTERFACEVERSION_VENGINESERVER_VERSION_21, NULL);
	Msg( "IVEngineServer *engine: 0x%x\n", engine );

	LUA->PushSpecial( SPECIAL_GLOB ); // _G
		LUA->PushCFunction( checkent );
		LUA->SetField( -2, "xyxenthing" );
	LUA->Pop();

	return 0;
}


GMOD_MODULE_CLOSE() {
	Msg( "fgt_xyxenthing gmod_module_close\n" );

	LUA->PushSpecial( SPECIAL_GLOB ); // _G
		LUA->PushNil();
		LUA->SetField( -2, "xyxenthing" );
	LUA->Pop();

	return 0;
}
